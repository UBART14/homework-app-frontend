import React from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function CreateHomeworkForm(props) {
  return (
    <>
      <br />
      <div className="card">
        <div className="card-body shadow">
          <h5 className="card-title">Create new Homework</h5>
          <ToastContainer autoClose={2000} />
          <div>
            <form>
              <div className="form-group row">
                <label className="col-sm-3 col-form-label mt-1">Title</label>
                <div className="col-sm-9">
                  <input
                    type="text"
                    name="title"
                    className="form-control"
                    placeholder="Title"
                    onChange={props.handleInput}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label className="col-sm-3 col-form-label mt-1">
                  Objectives
                </label>
                <div className="col-sm-9">
                  <input
                    type="text"
                    name="objectives"
                    className="form-control"
                    placeholder="Objectives"
                    onChange={props.handleInput}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label className="col-sm-3 col-form-label mt-1">Deadline</label>
                <div className="col-sm-9">
                  <input
                    type="datetime-local"
                    name="deadline"
                    className="form-control"
                    placeholder="Deadline"
                    onChange={props.handleInput}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label className="col-sm-3 col-form-label mt-1">
                  Assign To
                </label>
                <div className="col-sm-9">
                  <input
                    type="text"
                    name="assignedTo"
                    className="form-control"
                    placeholder="Student username"
                    onChange={props.handleInput}
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-sm-10">
                  <button
                    type="submit"
                    className="btn btn-primary"
                    onClick={props.handleSubmit}
                  >
                    Create
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default CreateHomeworkForm;
