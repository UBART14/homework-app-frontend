import React from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function ProfileUpdateForm(props) {
  return (
    <div className="card rounded shadow ml-1 mr-2 my-3 px-3 py-3">
      <h3>Update Details</h3>
      <ToastContainer autoClose={2000} />
      <form>
        <div className="form-group row py-1">
          <label className="col-sm-2 col-form-label">Update Name</label>
          <div className="col-sm-10">
            <input
              type="text"
              name="name"
              className="form-control"
              placeholder="Name"
              onChange={props.handleInput}
            />
          </div>
        </div>
        <div className="form-group row py-1">
          <label className="col-sm-2 col-form-label">New Password</label>
          <div className="col-sm-10">
            <input
              type="password"
              name="password"
              className="form-control"
              placeholder="Password"
              onChange={props.handleInput}
            />
          </div>
        </div>
        <div className="form-group row py-1">
          <label className="col-sm-2 col-form-label">Retype new Password</label>
          <div className="col-sm-10">
            <input
              type="password"
              name="retypePassword"
              className="form-control"
              placeholder="Password"
              onChange={props.handleInput}
            />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-sm-10">
            <button
              type="button"
              className="btn btn-primary"
              onClick={props.handleSubmit}
            >
              Update
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

export default ProfileUpdateForm;
