export const homeworkPromiseToast = (entitiy, message) => {
  return {
    pending: entitiy + " is being " + message,
    success: entitiy + " " + message + " successfully 👌",
    error: "Something went wrong 🤯",
  };
};
