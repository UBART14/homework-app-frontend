import React, { useState } from "react";
import { Collapse } from "react-collapse";
import "react-confirm-alert/src/react-confirm-alert.css";
import { MdDone } from "react-icons/md";
import { AiOutlineUndo } from "react-icons/ai";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";

function StudentHomeworkCard(props) {
  const [open, setOpen] = useState(false);

  return (
    <div>
      <ToastContainer autoClose={2000} />
      <div className="card mb-2 shadow">
        <div
          className="card-body rounded py-2"
          style={{ backgroundColor: props.color }}
        >
          <div className="row">
            <div className="col-9">
              <h5
                className="card-title text-light"
                onClick={() => setOpen(!open)}
              >
                {props.title}
              </h5>
            </div>
            <div className="col-3">
              <div className="d-flex justify-content-end">
                <button
                  className="btn mx-1"
                  onClick={() => props.confirmButton(props.id, props.title)}
                >
                  {props.status === "Not Finished" ? (
                    <MdDone />
                  ) : (
                    <AiOutlineUndo />
                  )}
                </button>
              </div>
            </div>
          </div>

          <Collapse isOpened={open}>
            <div className="text-light">
              <div className="row">
                <div className="col-sm-2">
                  <p>Objetives</p>
                </div>
                <div className="col-md-10">
                  <p>- {props.objectives}</p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-2">
                  <p>Assigned By</p>
                </div>
                <div className="col-md-10">
                  <p>- {props.assignedBy}</p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-2">
                  <p>Deadline</p>
                </div>
                <div className="col-md-10">
                  <p>
                    -{" "}
                    {moment(props.deadline).format(
                      "dddd, MMMM Do YYYY, h:mm:ss a"
                    )}
                  </p>
                </div>
              </div>
            </div>
          </Collapse>
        </div>
      </div>
    </div>
  );
}

export default StudentHomeworkCard;
