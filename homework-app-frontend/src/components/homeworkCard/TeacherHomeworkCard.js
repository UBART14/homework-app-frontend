import React, { useState } from "react";
import { Collapse } from "react-collapse";
import "react-confirm-alert/src/react-confirm-alert.css";
import { AiFillDelete, AiFillEdit } from "react-icons/ai";
import "react-toastify/dist/ReactToastify.css";
import Popover from "@mui/material/Popover";
import Typography from "@mui/material/Typography";
import moment from "moment";
import UpdateHomeworkForm from "../forms/UpdateHomeworkForm";

function TeacherHomeworkCard(props) {
  const [open, setOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const openPopover = Boolean(anchorEl);
  const id = openPopover ? "simple-popover" : undefined;

  const changeColor = (status) => {
    let color = "yellow";
    if (status === "Finished") {
      color = "red";
    } else if (status === "Not Finished") {
      color = "yellow";
    }
    return color;
  };

  return (
    <div>
      <div className="card mb-2 shadow">
        <div
          className="card-body rounded py-2"
          style={{ backgroundColor: changeColor(props.status) }}
        >
          <div className="row">
            <div className="col-9">
              <h5 className="card-title" onClick={() => setOpen(!open)}>
                {props.title}
              </h5>
            </div>
            <div className="col-3">
              <div className="d-flex justify-content-end">
                <button
                  className="btn btn-warning mx-1"
                  aria-describedby={id}
                  variant="contained"
                  onClick={handleClick}
                >
                  <AiFillEdit />
                </button>
                <Popover
                  id={id}
                  open={openPopover}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                >
                  <Typography sx={{ p: 2 }} component={"div"}>
                    <UpdateHomeworkForm
                      id={props.id}
                      title={props.title}
                      objectives={props.objectives}
                      deadline={moment(props.deadline).format(
                        "YYYY-MM-DDTkk:mm"
                      )}
                      assignedTo={props.assignedTo}
                    />
                  </Typography>
                </Popover>

                <button
                  className="btn btn-danger mx-1"
                  onClick={() =>
                    props.deleteHomeworkConfirm(props.id, props.title)
                  }
                >
                  <AiFillDelete />
                </button>
              </div>
            </div>
          </div>

          <Collapse isOpened={open}>
            <div className="row">
              <div className="col-2">
                <p>Objetives</p>
                <p>Assigned to</p>
                <p>Deadline</p>
                <p>status</p>
                <p>
                  <i>Created At</i>
                </p>
              </div>
              <div className="col-10">
                <p>- {props.objectives} </p>
                <p>- {props.assignedTo}</p>
                <p>
                  -{" "}
                  {moment(props.deadline).format(
                    "dddd, MMMM Do YYYY, h:mm:ss a"
                  )}
                </p>
                <p>- {props.status}</p>
                <p>
                  <i>
                    -{" "}
                    {moment(props.createdAt).format(
                      "dddd, MMMM Do YYYY, h:mm:ss a"
                    )}
                  </i>
                </p>
              </div>
            </div>
          </Collapse>
        </div>
      </div>
    </div>
  );
}

export default TeacherHomeworkCard;
