import axiosConfig from "../config/axiosConfig";

export const getCurrentlyLoggedInUser = () => {
  return axiosConfig.get("/users/user");
};

export const updateUser = (userId, updateData) => {
  return axiosConfig.put("/users/user/" + userId, updateData);
};
