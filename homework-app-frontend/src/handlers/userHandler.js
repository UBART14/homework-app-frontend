export const handleGetCurrentlyLoggedInUserLoading = () => {
  return true;
};

export const handleGetCurrentlyLoggedInUserSuccess = (response) => {
  return response.data;
};

export const handleGetCurrentlyLoggedInUserUnsuccess = (error) => {
  return {
    message: error,
    status: error.response.status,
  };
};

export const handleUpdateUserLoading = () => {
  return true;
};

export const handleUpdateUserSuccess = (response) => {
  return response.data;
};

export const handleUpdateUserUnsuccess = (error) => {
  return {
    message: error,
    status: error.response.status,
  };
};
