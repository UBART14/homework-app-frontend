import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import ProfileUpdateForm from "../../components/forms/ProfileUpdateForm";
import {
  getCurrentlyLoggedInUserAction,
  UpdateUserAction,
} from "../../redux/user/userActions";

function Profile() {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.loggedInUser);
  const [userData, setUserData] = useState({
    name: "",
    password: "",
    retypePassword: "",
  });

  useEffect(() => {
    dispatch(getCurrentlyLoggedInUserAction());
  }, []);

  const handleSubmit = () => {
    if (userData.password === "" && userData.retypePassword === "") {
      const updateData = {
        pName: userData.name,
      };
      dispatch(UpdateUserAction(user.id, updateData));
    } else if (userData.password !== userData.retypePassword) {
      alert("passwords dont match");
    } else {
      const updateData = {
        pName: userData.name,
        pPassword: userData.password,
      };
      dispatch(UpdateUserAction(user.id, updateData));
    }
  };

  const handleInput = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    setUserData((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-6">
          <h1 className="mx-2 my-2">Hello {user.name}! </h1>
        </div>
        <div className="col-6 d-flex justify-content-end">
          <h1 className="mx-2 my-2">
            {user.role === "teacher" ? "Teacher " : "Student"} Account
          </h1>
        </div>
      </div>

      <div className="container-fluid">
        <div className="row">
          <div className="col-6">
            <div className="">
              <div className="card rounded shadow ml-2 mr-1 my-3 px-3 py-3">
                <h3>Your Current Details</h3>
                <form>
                  <div className="form-group row py-1">
                    <label className="col-sm-2 col-form-label">Username</label>
                    <div className="col-sm-10">
                      <input
                        value={user.username}
                        type="text"
                        className="form-control"
                        disabled={true}
                      />
                    </div>
                  </div>
                  <div className="form-group row py-1">
                    <label className="col-sm-2 col-form-label">Name</label>
                    <div className="col-sm-10">
                      <input
                        value={user.name}
                        type="text"
                        className="form-control"
                        disabled={true}
                      />
                    </div>
                  </div>
                  <div className="form-group row py-1">
                    <label className="col-sm-2 col-form-label">Role</label>
                    <div className="col-sm-10">
                      <input
                        value={user.role}
                        type="text"
                        className="form-control"
                        disabled={true}
                      />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="col-6">
            <ProfileUpdateForm
              handleInput={handleInput}
              handleSubmit={handleSubmit}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;
