import React from "react";
import StudentHome from "./studentHome/StudentHome";
import TeacherHome from "./teacherHome/TeacherHome";
import Login from "../login/Login";
import {
  areTokensAvailable,
  getCurrentUserRole,
  getCurrentUsername,
} from "../../tokenDecode/tokenDecode";

function Home() {
  return (
    <div>
      {areTokensAvailable ? (
        getCurrentUserRole() === "teacher" ? (
          <TeacherHome username={getCurrentUsername()} />
        ) : (
          <StudentHome username={getCurrentUsername()} />
        )
      ) : (
        <Login />
      )}
    </div>
  );
}

export default Home;
