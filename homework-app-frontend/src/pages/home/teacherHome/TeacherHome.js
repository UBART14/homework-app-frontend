import React, { useEffect, useState } from "react";
import TeacherHeader from "../../../components/header/TeacherHeader";
import { useSelector, useDispatch } from "react-redux";
import {
  createNewHomeworkAction,
  deleteHomeworkByIdAction,
  getHomeworksByCreatedTeacherAction,
} from "../../../redux/homework/homeworkActions";
import TeacherHomeworkCard from "../../../components/homeworkCard/TeacherHomeworkCard";
import CreateHomeworkForm from "../../../components/forms/CreateHomeworkForm";
import { confirmAlert } from "react-confirm-alert";
import { OptionsTwoButtons } from "../../../components/alert/confirmOptions";

function TeacherHome(props) {
  const homeworks = useSelector((state) => state.homeworks);
  const [inputs, setInputs] = useState({});
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getHomeworksByCreatedTeacherAction());
  }, []);

  const handleInput = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    setInputs((prev) => ({ ...prev, [name]: value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const homeworkData = {
      pTitle: inputs.title,
      pAssignedTo: inputs.assignedTo,
      pDeadline: inputs.deadline,
      pObjectives: inputs.objectives,
    };
    dispatch(createNewHomeworkAction(homeworkData));
  };

  const deleteHomeworkConfirm = (homeworkId, title) => {
    confirmAlert(
      OptionsTwoButtons(
        homeworkId,
        "Delete " + title + "?",
        "Are you sure want to delete this?",
        "Yes",
        (homeworkId) => dispatch(deleteHomeworkByIdAction(homeworkId)),
        "No",
        () => {}
      )
    );
  };

  return (
    <>
      <TeacherHeader username={props.username} />
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-8">
            <h1 className="mx-2 my-2">Homeworks created by you</h1>
            <br />
            {homeworks.data &&
              homeworks.data.map((homework) => (
                <TeacherHomeworkCard
                  key={homework.id}
                  id={homework.id}
                  status={homework.status}
                  title={homework.title}
                  objectives={homework.objectives}
                  assignedTo={homework.assignedTo}
                  deadline={homework.deadline}
                  createdAt={homework.createdAt}
                  deleteHomeworkConfirm={deleteHomeworkConfirm}
                />
              ))}
          </div>

          <div className="col-lg-4">
            <CreateHomeworkForm
              handleInput={handleInput}
              handleSubmit={handleSubmit}
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default TeacherHome;
