import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import StudentHeader from "../../../components/header/StudentHeader";
import StudentHomeworkCard from "../../../components/homeworkCard/StudentHomeworkCard";
import {
  getHomeworkByAssignedStudentAction,
  updateHomeworkStatusAction,
} from "../../../redux/homework/homeworkActions";
import moment from "moment";
import { confirmAlert } from "react-confirm-alert";
import { OptionsTwoButtons } from "../../../components/alert/confirmOptions";

function StudentHome(props) {
  const homeworks = useSelector((state) => state.homeworks);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getHomeworkByAssignedStudentAction());
  }, []);

  const finishHomeworkConfirm = (homeworkId, title) => {
    confirmAlert(
      OptionsTwoButtons(
        homeworkId,
        "Finish " + title + "?",
        "Are you sure everything is finished?",
        "Yes",
        (homeworkId) =>
          dispatch(
            updateHomeworkStatusAction(homeworkId, { status: "Finished" })
          ),
        "No",
        () => {}
      )
    );
  };

  const undoFinishHomeworkConfirm = (homeworkId, title) => {
    confirmAlert(
      OptionsTwoButtons(
        homeworkId,
        "Undo finish " + title + "?",
        "Are you sure you want to undo?",
        "Yes",
        (homeworkId) =>
          dispatch(
            updateHomeworkStatusAction(homeworkId, { status: "Not Finished" })
          ),
        "No",
        () => {}
      )
    );
  };

  return (
    <div>
      <StudentHeader username={props.username} />
      <div className="container-fluid">
        <div className="row">
          <div className="px-3 col-lg-4">
            <h1 className="mx-2 my-2">Homeworks To Do</h1>
            <br />
            {homeworks.data
              .filter(
                (homework) =>
                  moment(homework.deadline) > moment() &&
                  homework.status === "Not Finished"
              )
              .map((homework) => (
                <StudentHomeworkCard
                  key={homework.id}
                  id={homework.id}
                  color="#17a2b8"
                  confirmButton={finishHomeworkConfirm}
                  status={homework.status}
                  title={homework.title}
                  objectives={homework.objectives}
                  assignedBy={homework.assignedBy}
                  deadline={homework.deadline}
                  createdAt={homework.createdAt}
                />
              ))}
          </div>
          <div className="px-3 col-lg-4">
            <h1 className="mx-2 my-2">Finished Homeworks</h1>
            <br />
            {homeworks.data
              .filter((homework) => homework.status === "Finished")
              .map((homework) => (
                <StudentHomeworkCard
                  key={homework.id}
                  id={homework.id}
                  color="#42ba96"
                  confirmButton={undoFinishHomeworkConfirm}
                  status={homework.status}
                  title={homework.title}
                  objectives={homework.objectives}
                  assignedBy={homework.assignedBy}
                  deadline={homework.deadline}
                  createdAt={homework.createdAt}
                />
              ))}
          </div>
          <div className="px-3 col-lg-4">
            <h1 className="mx-2 my-2">Overdue Homeworks</h1>
            <br />
            {homeworks.data
              .filter((homework) => moment(homework.deadline) < moment())
              .map((homework) => (
                <StudentHomeworkCard
                  key={homework.id}
                  id={homework.id}
                  color="#F32013"
                  title={homework.title}
                  objectives={homework.objectives}
                  assignedBy={homework.assignedBy}
                  deadline={homework.deadline}
                  createdAt={homework.createdAt}
                />
              ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default StudentHome;
