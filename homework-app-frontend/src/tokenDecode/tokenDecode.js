import jwt_decode from "jwt-decode";

export const getCurrentUserRole = () => {
  return jwt_decode(localStorage.getItem("access_token")).roles[0];
};

export const getCurrentUsername = () => {
  return jwt_decode(localStorage.getItem("access_token")).sub;
};

export const areTokensAvailable = () => {
  if (
    localStorage.getItem("access_token") === null ||
    localStorage.getItem("refresh_token") === null
  ) {
    return false;
  } else {
    return false;
  }
};
