import {
  createNewHomework,
  deleteHomeworkById,
  getHomeworkByAssignedStudent,
  getHomeworksByCreatedTeacher,
  updateHomeworkById,
  updateHomeworkStatusById,
} from "../../api/homeworkAPI";
import {
  handleCreateNewHomeworkLoadingRequest,
  handleCreateNewHomeworkSuccessResponse,
  handleCreateNewHomeworkUnsuccessResponse,
  handleGetHomeworksByAssignedStudentLoadingRequest,
  handleGetHomeworksByAssignedStudentSuccessResponse,
  handleGetHomeworksByAssignedStudentUnsuccessResponse,
  handleGetHomeworksByCreatedTeacherLoadingRequest,
  handleGetHomeworksByCreatedTeacherSuccessResponse,
  handleGetHomeworksByCreatedTeacherUnsuccessResponse,
  handleHomeworkDeleteLoadingRequest,
  handleHomeworkDeleteSuccessResponse,
  handleHomeworkDeleteUnsuccessResponse,
  handleHomeworkStatusUpdateLoading,
  handleHomeworkStatusUpdateSuccess,
  handleHomeworkStatusUpdateUnsuccess,
  handleHomeworkUpdateLoading,
  handleHomeworkUpdateSuccess,
  handleHomeworkUpdateUnsuccess,
} from "../../handlers/homerworkHandler";
import {
  CREATE_NEW_HOMEWORK_LOADING,
  CREATE_NEW_HOMEWORK_SUCCESS,
  CREATE_NEW_HOMEWORK_UNSUCCESS,
  DELETE_HOMEWORK_LOADING,
  DELETE_HOMEWORK_SUCCESS,
  DELETE_HOMEWORK_UNSUCCESS,
  GET_HOMEWORK_BY_ASSIGNED_STUDENT_LOADING,
  GET_HOMEWORK_BY_ASSIGNED_STUDENT_SUCCESS,
  GET_HOMEWORK_BY_CREATED_TEACHER_LOADING,
  GET_HOMEWORK_BY_CREATED_TEACHER_SUCCESS,
  GET_HOMEWORK_BY_CREATED_TEACHER_UNSUCCESS,
  UPDATE_HOMEWORK_LOADING,
  UPDATE_HOMEWORK_STATUS_BY_ID_LOADING,
  UPDATE_HOMEWORK_STATUS_BY_ID_SUCCESS,
  UPDATE_HOMEWORK_STATUS_BY_ID_UNSUCESS,
  UPDATE_HOMEWORK_SUCCESS,
  UPDATE_HOMEWORK_UNSUCCESS,
} from "./homeworkActionTypes";
import { toast } from "react-toastify";
import { homeworkPromiseToast } from "../../components/toasts/promiseToasts";

export const getHomeworksByCreatedTeacherAction = () => {
  return async (dispatch) => {
    dispatch({
      type: GET_HOMEWORK_BY_CREATED_TEACHER_LOADING,
      payload: handleGetHomeworksByCreatedTeacherLoadingRequest(),
    });
    try {
      let response = await getHomeworksByCreatedTeacher();
      dispatch({
        type: GET_HOMEWORK_BY_CREATED_TEACHER_SUCCESS,
        payload: handleGetHomeworksByCreatedTeacherSuccessResponse(response),
      });
    } catch (error) {
      dispatch({
        type: GET_HOMEWORK_BY_CREATED_TEACHER_UNSUCCESS,
        payload: handleGetHomeworksByCreatedTeacherUnsuccessResponse(error),
      });
    }
  };
};

export const createNewHomeworkAction = (homeworkData) => {
  return async (dispatch) => {
    dispatch({
      type: CREATE_NEW_HOMEWORK_LOADING,
      payload: handleCreateNewHomeworkLoadingRequest(),
    });
    try {
      let response = await toast.promise(
        createNewHomework(homeworkData),
        homeworkPromiseToast("Homework", "created")
      );
      dispatch({
        type: CREATE_NEW_HOMEWORK_SUCCESS,
        payload: handleCreateNewHomeworkSuccessResponse(response),
      });
    } catch (error) {
      dispatch({
        type: CREATE_NEW_HOMEWORK_UNSUCCESS,
        payload: handleCreateNewHomeworkUnsuccessResponse(error),
      });
    }
  };
};

export const deleteHomeworkByIdAction = (homeworkId) => {
  return async (dispatch) => {
    dispatch({
      type: DELETE_HOMEWORK_LOADING,
      payload: handleHomeworkDeleteLoadingRequest(),
    });
    try {
      let response = await toast.promise(
        deleteHomeworkById(homeworkId),
        homeworkPromiseToast("Homework", "deleted")
      );
      dispatch({
        type: DELETE_HOMEWORK_SUCCESS,
        payload: handleHomeworkDeleteSuccessResponse(response),
      });
    } catch (error) {
      dispatch({
        type: DELETE_HOMEWORK_UNSUCCESS,
        payload: handleHomeworkDeleteUnsuccessResponse(error),
      });
    }
  };
};

export const updateHomeworkAction = (homeworkId, homeworkData) => {
  return async (dispatch) => {
    dispatch({
      type: UPDATE_HOMEWORK_LOADING,
      payload: handleHomeworkUpdateLoading(),
    });
    try {
      let response = await toast.promise(
        updateHomeworkById(homeworkId, homeworkData),
        homeworkPromiseToast("Homework", "updated")
      );
      dispatch({
        type: UPDATE_HOMEWORK_SUCCESS,
        payload: handleHomeworkUpdateSuccess(response),
      });
    } catch (error) {
      dispatch({
        type: UPDATE_HOMEWORK_UNSUCCESS,
        payload: handleHomeworkUpdateUnsuccess(error),
      });
    }
  };
};

export const updateHomeworkStatusAction = (homeworkId, status) => {
  return async (dispatch) => {
    dispatch({
      type: UPDATE_HOMEWORK_STATUS_BY_ID_LOADING,
      payload: handleHomeworkStatusUpdateLoading(),
    });
    try {
      let response = await toast.promise(
        updateHomeworkStatusById(homeworkId, status),
        homeworkPromiseToast("Homework status", "updated")
      );
      dispatch({
        type: UPDATE_HOMEWORK_STATUS_BY_ID_SUCCESS,
        payload: handleHomeworkStatusUpdateSuccess(response),
      });
    } catch (error) {
      dispatch({
        type: UPDATE_HOMEWORK_STATUS_BY_ID_UNSUCESS,
        payload: handleHomeworkStatusUpdateUnsuccess(error),
      });
    }
  };
};

export const getHomeworkByAssignedStudentAction = () => {
  return async (dispatch) => {
    dispatch({
      type: GET_HOMEWORK_BY_ASSIGNED_STUDENT_LOADING,
      payload: handleGetHomeworksByAssignedStudentLoadingRequest(),
    });
    try {
      let response = await getHomeworkByAssignedStudent();
      dispatch({
        type: GET_HOMEWORK_BY_ASSIGNED_STUDENT_SUCCESS,
        payload: handleGetHomeworksByAssignedStudentSuccessResponse(response),
      });
    } catch (error) {
      dispatch({
        type: GET_HOMEWORK_BY_CREATED_TEACHER_UNSUCCESS,
        payload: handleGetHomeworksByAssignedStudentUnsuccessResponse(error),
      });
    }
  };
};
