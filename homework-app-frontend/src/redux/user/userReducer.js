import {
  GET_CURRENTLY_LOGGED_IN_USER_LOADING,
  GET_CURRENTLY_LOGGED_IN_USER_SUCCESS,
  GET_CURRENTLY_LOGGED_IN_USER_UNSUCCESS,
  UPDATE_USER_LOADING,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_UNSUCCESS,
} from "./userActionTypes";

const initialUserState = {
  isLoading: false,
  loggedInUser: {},
  data: [],
  error: {
    status: "",
    message: "",
  },
};

const userReducer = (state = initialUserState, action) => {
  switch (action.type) {
    case GET_CURRENTLY_LOGGED_IN_USER_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case GET_CURRENTLY_LOGGED_IN_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        loggedInUser: action.payload,
      };
    case GET_CURRENTLY_LOGGED_IN_USER_UNSUCCESS:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    case UPDATE_USER_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        loggedInUser: action.payload,
      };
    case UPDATE_USER_UNSUCCESS:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;
