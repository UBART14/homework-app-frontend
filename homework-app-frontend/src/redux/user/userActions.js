import { getCurrentlyLoggedInUser, updateUser } from "../../api/userAPI";
import { homeworkPromiseToast } from "../../components/toasts/promiseToasts";
import {
  handleGetCurrentlyLoggedInUserLoading,
  handleGetCurrentlyLoggedInUserSuccess,
  handleGetCurrentlyLoggedInUserUnsuccess,
  handleUpdateUserLoading,
  handleUpdateUserSuccess,
  handleUpdateUserUnsuccess,
} from "../../handlers/userHandler";
import {
  GET_CURRENTLY_LOGGED_IN_USER_LOADING,
  GET_CURRENTLY_LOGGED_IN_USER_SUCCESS,
  GET_CURRENTLY_LOGGED_IN_USER_UNSUCCESS,
  UPDATE_USER_LOADING,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_UNSUCCESS,
} from "./userActionTypes";
import { toast } from "react-toastify";

export const getCurrentlyLoggedInUserAction = () => {
  return async (dispatch) => {
    dispatch({
      type: GET_CURRENTLY_LOGGED_IN_USER_LOADING,
      payload: handleGetCurrentlyLoggedInUserLoading(),
    });
    try {
      let response = await getCurrentlyLoggedInUser();
      dispatch({
        type: GET_CURRENTLY_LOGGED_IN_USER_SUCCESS,
        payload: handleGetCurrentlyLoggedInUserSuccess(response),
      });
    } catch (error) {
      dispatch({
        type: GET_CURRENTLY_LOGGED_IN_USER_UNSUCCESS,
        payload: handleGetCurrentlyLoggedInUserUnsuccess(error),
      });
    }
  };
};

export const UpdateUserAction = (userId, updateData) => {
  return async (dispatch) => {
    dispatch({
      type: UPDATE_USER_LOADING,
      payload: handleUpdateUserLoading(),
    });
    try {
      let response = await toast.promise(
        updateUser(userId, updateData),
        homeworkPromiseToast("User", "updated")
      );
      dispatch({
        type: UPDATE_USER_SUCCESS,
        payload: handleUpdateUserSuccess(response),
      });
    } catch (error) {
      dispatch({
        type: UPDATE_USER_UNSUCCESS,
        payload: handleUpdateUserUnsuccess(error),
      });
    }
  };
};
