import { applyMiddleware, createStore, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import authReducer from "./auth/authReducer";
import homeworkReducer from "./homework/homeworkReducer";
import userReducer from "./user/userReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  homeworks: homeworkReducer,
  user: userReducer,
});

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

export default store;
